import React, { useState, useEffect, useRef } from 'react';
import Name from './Name';
import randomColor from 'randomcolor';
import ColorPicker from './ColorPicker';



function Paint() {
    const [colors, setColors] = useState([]);
    const [activeColor, setActiveColor] = useState(null);
    const headerRef = useRef({ offsetHeight: 0 });

    const getColors = () => {
        const baseColor = randomColor().slice(1);
        fetch(`https://www.thecolorapi.com/scheme?hex=${baseColor}&mode=monochrome`)
            .then(res => res.json())
            .then(res => {
                setColors(res.colors.map(color => color.hex.value));
                setActiveColor(res.colors[0].hex.value);
            });
    };
    useEffect(getColors, []);

    return(
        <div className='app'>
            <header ref={headerRef} style={{ borderTop: `10px solid ${activeColor}`}}>
                <div>
                    <Name />
                </div>
                <div style={{ marginTop: 10 }}>
                    <ColorPicker
                        colors={colors}
                        activeColor={activeColor}
                        setActiveColor={setActiveColor}
                    />
                </div>
            </header>
            {activeColor && (
                <Canvas
                    color={activeColor}
                    height={window.innerHeight - headerRef.current.offsetHeight}
                />
            )}
        </div>
    )
}





export default Paint;
